# SWADE Test Drive

Conversion of: [_Deadlands the Weird West: Blood on the Range_ (Savage Worlds Test Drive #SWADE)](https://www.peginc.com/store/deadlands-the-weird-west-blood-on-the-range-savage-worlds-test-drive-swade/)

*Deadlands*, *Savage Worlds*, all unique characters, creatures, artwork, logos, trademarks, and the Pinnacle logo are &copy; 2020 Pinnacle Entertainment Group. All Rights Reserved.

## Package Description

Welcome to the official _Deadlands the Weird West: Blood on the Range (Savage Worlds Test Drive #SWADE)_ free premium content pack for Foundry Virtual Tabletop.

Have you ever wanted to try a game of _Savage Worlds_? This absolutely free Adventure version of the Savage Worlds Test Drive rules includes everything you need to play:

- 6 pregenerated “archetype” characters as actors fully statted up with all their gear and tokens
- 3 included NPCs as preconfigured actors with their gear and tokens
- 7 powers ready for use 
- Interlinked Journal Entries of the whole text detailing rules for: Basics, Combat, and Powers
- Bonus map as a Scene lit and ready for the tokens!

It also includes _Blood on the Range_, a brand new tale of death on the high plains for [_The Horror at Headstone Hill_](https://www.peginc.com/store/deadlands-the-horror-at-headstone-hill-boxed-set/), the latest campaign set in [_Deadlands: the Weird West_](https://www.peginc.com/store/deadlands-the-weird-west-core-rules/) by Savage Worlds’ creator Shane Lacy Hensley!

Once you’ve tried the _Test Drive_, purchase the [Savage Worlds Adventure Edition core rules here](https://foundryvtt.com/packages/swade-core-rules), and your [Deadlands: the Weird West items here](https://www.peginc.com/product-category/deadlands-weird-west/)!

**_Everything you need to play is in this FREE Module!_ Savage Worlds Adventure Edition, Headstone Hill _or_ Deadlands: the Weird West _core rules are NOT required to play this adventure._**

## Unlocking the Module

- Install the module from Module Browser in the Foundry VTT application

*Savage Worlds* is available from Pinnacle Entertainment Group at [www.peginc.com](http://www.peginc.com).  This Adventure was prepared under the direction of Dr. Amy Bliss Marshall by the [Sigil Entertainment Group](http://sigil.info/) VTT Conversion Team, including: Joe Schnurr, Josh Olsen, Chris Valentine, Kevin Dreßler, and John Stevens.
